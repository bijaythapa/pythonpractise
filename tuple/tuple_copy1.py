dimensions = (100, 20, 20, 100)
print("Original dimension: ")
for dimension in dimensions:
    print(dimension)

dimensions = (100, 20)
print("\nModified dimension: ")
for dimension in dimensions:
    print(dimension)
