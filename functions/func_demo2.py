def make_pizza(*toppings):
    """Print the list of toppings that have been requested."""
    print(toppings)
    """Summarize the pizza we are about to make."""
    print("Making pizza with toppings: ")
    for topping in toppings:
        print("- "+topping)


def make_pizza_size(size, *toppings):
    print("\nMaking a "+str(size)+"-inch pizza with following toppings: ")
    for topping in toppings:
        print("- "+topping)


# make_pizza('Pepperoni')
# make_pizza('hawaii', 'chicken', 'veggie')
make_pizza('mushroom', 'green peppers', 'extra cheese')
make_pizza_size(12, 'hawaii', 'chicken', 'veggie')
