def make_pizza(*toppings):
    """Summarize the pizza we are about to make."""
    print("Making pizza with toppings: ")
    for topping in toppings:
        print("- "+topping)


def make_pizza_size(size, *toppings):
    """Summarize the pizza we are about to make."""
    print("\nMaking a "+str(size)+"-inch pizza with following toppings: ")
    for topping in toppings:
        print("- "+topping)