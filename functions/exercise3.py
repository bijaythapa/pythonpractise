def city_country(city_name, country):
    return f"{city_name}, {country}"


info = city_country('Kathmandu', 'Nepal')
print(info)


def make_album(artist_name, album_title, no_tracks):
    about_album = {
        artist_name: artist_name,
        album_title: album_title,
    }
    if no_tracks:
        about_album['tracks'] = no_tracks
    return about_album


albums = []
# flag = True
while True:
    alb_name = input("Enter Album name: ")
    art_name = input("Enter Artist name: ")
    tracks = int(input("Enter no of tracks: "))
    albums.append(make_album(art_name, alb_name, tracks))
    flg = input("Wanna continue? (y/n): ")
    if flg == 'n':
        # flag = False
        break

print("Your Album Lists: ")
for album in albums:
    print(album)
