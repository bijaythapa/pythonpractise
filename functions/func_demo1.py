def display_func(msg):
    print(msg)


message = "Hello from the other side !!"
display_func(message)


# Positional Arguments
def describe_pet(animal_type, pet_name):
    """Display information about a pet"""
    print("\nI have a " + animal_type + ".")
    print("My " + animal_type + "'s name is " + pet_name.title() + ".")


describe_pet('hamster', 'harry')
describe_pet('dog', 'black')


# Keyboard Arguments
def describe_pet(animal_type, pet_name):
    """Display information about a pet"""
    print("\nI have a " + animal_type + ".")
    print("My " + animal_type + "'s name is " + pet_name.title() + ".")


describe_pet(animal_type='dog', pet_name='fatty')
describe_pet(animal_type='hamster', pet_name='harry')
