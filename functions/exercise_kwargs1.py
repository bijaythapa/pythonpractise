def car_info(manufacturer, model, **kwargs):
    about_car = {
        'manufacturer': manufacturer,
        'model': model,
    }
    for title, info in kwargs.items():
        about_car[title] = info
    return about_car


car = car_info("Toyota", 'tot-456', color='red', tow_package='True')
print(car)