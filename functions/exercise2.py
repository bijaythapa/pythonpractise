def make_shirt(print_text, size='large'):
    print("Your size of shirt is: '" + size + "', printing text is: '" + print_text + "'.")


make_shirt(print_text='I Love Python')
make_shirt(print_text="I love Java", size="medium")


def cities(city_description, city_name='Kathmandu'):
    print(city_name + " is in " + city_description)


cities(city_description='Nepal')
cities(city_description='US', city_name='California')
