def car_info(manufacturer, model, **kwargs):
    about_car = {
        'manufacturer': manufacturer,
        'model': model,
    }
    for title, info in kwargs.items():
        about_car[title] = info
    return about_car


def ordered_sandwiches(*args):
    print("Ordered sandwiches are: ")
    for sandwich in args:
        print(sandwich)
