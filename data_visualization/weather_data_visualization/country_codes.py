from pygal_maps_world.i18n import COUNTRIES


def get_country_code(country_name):
    """Return Pygal 2-digit country code for the given country."""
    for code, name in COUNTRIES.items():
        # print(code + " : " + name)
        if country_name == name:
            return code
    return None
