import csv
import matplotlib.pyplot as plt
from datetime import datetime

# Get dates, high and low temperature from the file
# filename = 'sitka_weather_07-2014.csv'
# filename = 'sitka_weather_2014.csv'
filename = 'death_valley_2014.csv'
with open(filename, 'rb', encoding='unicode_escape') as file_object:
    reader = csv.reader(file_object)
    header_row = next(reader)
    print(header_row)
    # for index, column_header in enumerate(header_row):
    #     print(index, column_header)
    highs, lows, dates = [], [], []
    for row in reader:
        # print(row)
        try:
            high = int(row[15])
            low = int(row[14])
            current_date = datetime.strptime(row[0], "%Y-%m-%d")
        except ValueError:
            print(current_date, 'Missing Data')
        else:
            highs.append(high)
            lows.append(low)
            dates.append(current_date)

    # Plot data
    fig = plt.figure(dpi=128, figsize=(10, 6))
    plt.plot(dates, highs, c='red', alpha=0.5)
    plt.plot(dates, lows, c='blue', alpha=0.5)
    plt.fill_between(dates, highs, lows, facecolor='blue', alpha=0.1)

    # Format plot
    # title = "Daily High temperature - 2014"
    title = "Daily High temperature - 2014\nDeath Valley, CA"
    plt.title(title, fontsize=24)
    plt.xlabel('', fontsize=16)
    fig.autofmt_xdate()
    plt.ylabel("Temperature (F)", fontsize=16)
    plt.tick_params(axis='both', which='major', labelsize=16)

    # plt.savefig('high_low.jpg', bbox_inches='tight')
    plt.show()
