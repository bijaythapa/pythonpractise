import json
from pygal_maps_world.maps import World
from pygal.style import RotateStyle as RoSy, LightColorizedStyle as LiColSy
from data_visualization.weather_data_visualization.country_codes import get_country_code

filename = 'population_data.json'
with open(filename) as file_object:
    pop_data = json.load(file_object)

# Print the 2010 population of each country
# Build a Dictionary of population data that fits in world map.
cc_populations = dict()

for pop_dict in pop_data:
    if pop_dict['Year'] == '2010':
        country_name = pop_dict['Country Name']
        population = int(float(pop_dict['Value']))
        # print(country_name+" : "+str(population))
        code = get_country_code(country_name)
        if code:
            # print(code+" : "+str(population))
            cc_populations[code] = population

# Group Countries in 3 population levels.
cc_pops_1, cc_pops_2, cc_pops_3 = {}, {}, {}
for cc, pop in cc_populations.items():
    if pop < 10000000:
        cc_pops_1[cc] = pop
    elif pop < 1000000000:
        cc_pops_2[cc] = pop
    else:
        cc_pops_3[cc] = pop

# See how many countries in each level
print(len(cc_pops_1), len(cc_pops_2), len(cc_pops_3))

wm_style = RoSy('#FF9033', base_style=LiColSy)
world_map = World(style=wm_style)

world_map._title = "World Population in 2010, by Country"
world_map.add('<10m', cc_pops_1)
world_map.add('10m-1b', cc_pops_2)
world_map.add('>1b', cc_pops_3)

world_map.render_to_file('cc_populations.svg')
