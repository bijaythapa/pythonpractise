import matplotlib.pyplot as plt
import matplotlib.cm as c

x_data = list(range(1, 1001))
y_data = [x ** 2 for x in x_data]

# plt.scatter(x_data, y_data, c=y_data, cmap=plt.cm.Blues, edgecolors='none', s=40)
plt.scatter(x_data, y_data, c=y_data, cmap=c.Blues, edgecolors='none', s=40)
# plt.scatter(x_data, y_data, c=x_data, cmap=plt.cm.Blues, edgecolors='none', s=40)
plt.title("Square Numbers", fontsize=24)
plt.xlabel("Numbers", fontsize=14)
plt.ylabel("Squares", fontsize=14)
plt.tick_params(axis='both', which='major', labelsize=14)
plt.axis([0, 1100, 0, 1100000])
plt.show()
# plt.savefig('square_plot.png', bbox_inches='tight')
