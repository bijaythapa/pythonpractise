import matplotlib.pyplot as plt

x_label = list(range(1, 1001))
y_label = [x**3 for x in x_label]

# plt.plot(x_label, y_label, linewidth=5)
plt.scatter(x_label, y_label, c=y_label, cmap=plt.cm.Greens, edgecolors='none', s=100)
plt.title("Cubic Plot", fontsize=24)
plt.xlabel("Numbers", fontsize=14)
plt.xlabel("Cubes", fontsize=14)
plt.tick_params(axis='both', which='major', labelsize=14)
plt.axis([0, 1100, 0, 1100000000])
plt.show()
# plt.savefig('cube_plot.png', bbox_inches='tight')
