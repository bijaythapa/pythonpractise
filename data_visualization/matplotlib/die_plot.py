import matplotlib.pyplot as plt
from data_visualization.matplotlib.die import Die

die_3 = Die()
die_4 = Die(8)
outputs = list()
for roll in range(1000):
    result = die_3.roll()+die_4.roll()
    outputs.append(result)

roll_nums = list(range(1, (die_3.num_sides+die_4.num_sides+1)))
frequencies = []
for value in roll_nums:
    frequency = outputs.count(value)
    frequencies.append(frequency)

plt.title("Result of rolling a D"+str(die_3.num_sides+die_4.num_sides)+"die 1000 times")
plt.xlabel("Result")
plt.ylabel("Frequency")
plt.tick_params(axis='both', labelsize=14)
plt.plot(roll_nums, frequencies, linewidth=5)
plt.show()
