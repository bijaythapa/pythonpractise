import matplotlib.pyplot as plt
from data_visualization.matplotlib.die import Die

die_1 = Die()
die_2 = Die()
results = []

for x in range(1000):
    result = die_1.roll()+die_2.roll()
    results.append(result)

values = list(range(1, (die_1.num_sides+die_2.num_sides+1)))
frequencies = []

for value in values:
    frequency = results.count(value)
    frequencies.append(frequency)

plt.title("Result of rolling a D"+str(die_1.num_sides+die_2.num_sides)+"die 1000 times")
plt.xlabel("Result")
plt.ylabel("Frequency")
plt.tick_params(axis='both', labelsize=14)

plt.scatter(values, frequencies, c='red', edgecolors='none', s=100)
plt.show()
