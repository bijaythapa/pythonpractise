import matplotlib.pyplot as plt

# here 's=' is size
# plt.scatter(2, 4, s=400)

x_values = [1, 2, 3, 4, 5]
y_values = [1, 4, 9, 16, 25]
plt.scatter(x_values, y_values, s=100)

# set chart title and label axes
plt.title("Square numbers", fontsize=24)
plt.xlabel("Number", fontsize=14)
plt.ylabel("Square", fontsize=14)

# set the size of tick labels
plt.tick_params(axis='both', which='major', labelsize=14)

plt.show()
