import matplotlib.pyplot as plt
from data_visualization.matplotlib.random_walk import RandomWalk

# keep making random walk as long as the program is active
while True:
    # make a random walk and plot the points.
    rw = RandomWalk(5000)
    rw.fill_walk()

    # set size of plotting window
    # plt.figure(figsize=(14, 8))

    plt.title("'drunk Ant Walk'", fontsize=24)
    plt.xlabel("X-values", fontsize=14)
    plt.ylabel("Y-values", fontsize=14)
    plt.tick_params(axis='both', labelsize=14)

    point_numbers = list(range(rw.num_points))
    plt.scatter(rw.x_values, rw.y_values, c=point_numbers, cmap=plt.cm.Blues, edgecolors='none', s=8)

    # Emphasize the first and last point
    plt.scatter(0, 0, c='green', edgecolors='none', s=100)
    plt.scatter(rw.x_values[-1], rw.y_values[-1], c='red', edgecolors='none', s=100)

    # Remove the axes
    # plt.axes().get_xaxis().set_visible(False)
    # plt.axes().get_yaxis().set_visible(False)

    # plt.savefig('ran_walk.png', bbox_inches='tight')
    plt.show()

    keep_running = input("Make another walk? (Y/n): ")
    if keep_running == 'n':
        break
