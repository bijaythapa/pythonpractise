# import matplotlib.pyplot
import matplotlib.pyplot as plt

input_values = [1, 2, 3, 4, 5, 6]
squares = [1, 4, 9, 16, 25, 36]

# matplotlib.pyplot.plot(squares)
# plt.plot(squares, linewidth=5)
plt.plot(input_values, squares, linewidth=5)

# set title and label axes
plt.title("Square Numbers", fontsize=24)
plt.xlabel("Value", fontsize=14)
plt.ylabel("Square of value", fontsize=14)

# set size to tick label
plt.tick_params(axis="both", labelsize=14)

# matplotlib.pyplot.show()
plt.show()
