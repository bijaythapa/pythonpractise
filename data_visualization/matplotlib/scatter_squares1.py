import matplotlib.pyplot as plt

x_values = list(range(1, 1001))
y_values = [x**2 for x in x_values]

# edgecolor='none' removes edge color of the data points.
# c = 'color_name' // c = (r, g, b) to set custom color.
plt.scatter(x_values, y_values, c='red', edgecolors='none', s=40)
# plt.scatter(x_values, y_values, c=[0, 0, 0.8], edgecolors='none', s=40)
plt.title('Square Numbers', fontsize=24)
plt.xlabel('Numbers', fontsize=14)
plt.ylabel('Squares', fontsize=14)
plt.tick_params(axis='both', which='major', labelsize=14)

# to set range of each axis
plt.axis([0, 1100, 0, 1100000])
plt.show()
