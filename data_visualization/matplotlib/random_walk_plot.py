import matplotlib.pyplot as plt
from data_visualization.matplotlib.random_walk import RandomWalk

while True:
    random_walk = RandomWalk()
    random_walk.fill_walk()

    # no effect showed.
    # plt.plot(0, 0, c='green', linewidth=10)
    # plt.plot(random_walk.x_values[-1], random_walk.y_values[-1], c='red', linewidth=10)

    plt.plot(random_walk.x_values, random_walk.y_values, c='blue', linewidth=3)

    plt.axes().get_xaxis().set_visible(False)
    plt.axes().get_yaxis().set_visible(False)

    plt.show()

    keep_running = input("want to continue? (Y/n): ")
    if keep_running == 'n':
        break
