from pygal_maps_world.i18n import COUNTRIES


# for country_code in sorted(COUNTRIES.keys()):
#     print(country_code, COUNTRIES[country_code])

def get_country_code(country_name):
    """Return 2 digit country code given by Pygal."""
    for code, name in COUNTRIES.items():
        if country_name == name:
            return code
    return None
