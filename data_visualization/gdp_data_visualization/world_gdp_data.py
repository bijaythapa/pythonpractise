import json
from pygal_maps_world.maps import World
from pygal.style import LightColorizedStyle as LiCoS, RotateStyle as RoS
from data_visualization.gdp_data_visualization.countries import get_country_code

file_name = 'gdp_json.json'
with open(file_name) as obj:
    gdp_data = json.load(obj)

# get 2016 gdp data for each country
# Build dictionary of country code and name as Pygal world map accepts.
cc_world = dict()

for gdb_dict in gdp_data:
    if gdb_dict["Year"] == 2016:
        country_name = gdb_dict['Country Name']
        gdp_value = int(float(gdb_dict['Value']))
        c_code = get_country_code(country_name)
        if c_code:
            cc_world[c_code] = gdp_value


cc_gdp_1, cc_gdp_2, cc_gdp_3 = {}, {}, {}
for cc, value in cc_world.items():
    if 0 < value < 10000000000:
        cc_gdp_1[cc] = value
    elif 10000000000 < value < 100000000000:
        cc_gdp_2[cc] = value
    else:
        cc_gdp_3[cc] = value

# Print number of countries in the categories
print(str(len(cc_gdp_1))+" : "+str(len(cc_gdp_2))+" : "+str(len(cc_gdp_3)))

map_style = RoS("#092a59", base_style=LiCoS)
# map_style = LiCoS()
world_map = World(style=map_style)

world_map._title = "World GDP values- 2016, by Countries"
world_map.add('GDP: 0-e^10', cc_gdp_1)
world_map.add('GDP: e^10-e^11', cc_gdp_2)
world_map.add('GDP: >e^11', cc_gdp_3)

world_map.render_to_file('world_gdp.svg')
