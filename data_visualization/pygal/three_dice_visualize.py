import pygal
from data_visualization.pygal.die import Die

die_1 = Die()
die_2 = Die()
die_3 = Die()

results = []
for roll in range(2000):
    result = die_1.roll() + die_2.roll() + die_3.roll()
    results.append(result)

frequencies = []
max_value = die_1.num_sides + die_2.num_sides + die_3.num_sides
for data in range(3, (max_value + 1)):
    frequency = results.count(data)
    frequencies.append(frequency)

# hist = pygal.Pie()
hist = pygal.Bar()
hist._title = "Result of 3 dice rolling 2000 times"
hist.x_labels = [x for x in range(3, max_value + 1)]
hist._x_title = "Result"
hist._y_title = "Frequency"
hist.add("D6+D6+D6", frequencies)
hist.render_to_file("three_dice_visual.svg")
