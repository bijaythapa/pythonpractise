import pygal
from data_visualization.pygal.die import Die

die_1 = Die()
die_2 = Die(10)

# Make some rolls and store result in list
outputs = list()
for roll in range(1000):
    result = die_1.roll()+die_2.roll()
    outputs.append(result)

# Analyze the results
frequencies = []
max_value = die_1.num_sides + die_2.num_sides
for value in range(2, max_value+1):
    frequency = outputs.count(value)
    frequencies.append(frequency)

# Visualize the data
hist = pygal.Bar()
hist._title = "Result of rolling two dice, D6 and D10, 50000 times."
hist.x_labels = list(range(2, max_value+1))
hist._x_title = "Result"
hist._y_title = "Frequency"

hist.add('D6+D10', frequencies)
hist.render_to_file('different_dice_visual.svg')
