import pygal
from data_visualization.pygal.die import Die

# create a D6
die = Die()

# make some rolls and store the results in a list.
results = list()
for roll_num in range(1000):
    result = die.roll()
    results.append(result)

frequencies = list()
for value in range(1, die.num_sides + 1):
    frequency = results.count(value)
    frequencies.append(frequency)

# visualize the result
hist = pygal.Bar()

hist._title = "Result of rolling D6 die 1000 times"
# hist.x_labels = ['1', '2', '3', '4', '5', '6']
hist.x_labels = [x for x in range(1, die.num_sides+1)]
hist._x_title = "Result"
hist._y_title = "Frequency of Result"

hist.add('D6', frequencies)
# hist.render()
hist.render_to_file('die_visual.svg')
