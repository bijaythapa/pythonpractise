import pygal
from data_visualization.pygal.random_walk import RandomWalk

random_walk = RandomWalk()
random_walk.walk()
hist = pygal.Bar()
hist._title = "Result of random walk taking 500 steps"
hist.x_labels = random_walk.x_values
hist._x_title = "step to x"
hist._y_title = "step to y"

hist.add("Steps", random_walk.y_values)
hist.render_to_file("random_walk_visual.svg")
