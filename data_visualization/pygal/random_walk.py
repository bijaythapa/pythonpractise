from random import choice


class RandomWalk:
    """A class to generate random walk."""

    def __init__(self, num_points=50):
        self.num_points = num_points
        self.x_values = [0]
        self.y_values = [0]
        self.direction = [1, -1]
        self.distance = [0, 1, 2, 3, 4]

    def get_step(self):
        direction = choice(self.direction)
        distance = choice(self.distance)
        step = direction * distance
        return step

    def walk(self):
        """calculate all the points in the walk."""
        while len(self.x_values) < self.num_points:
            x_step = self.get_step()
            y_step = self.get_step()

            # reject move go no where
            if x_step == 0 and y_step == 0:
                continue

            next_x_step = self.x_values[-1]+x_step
            next_y_step = self.y_values[-1]+y_step

            self.x_values.append(next_x_step)
            self.y_values.append(next_y_step)