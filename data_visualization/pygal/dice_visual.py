import pygal
from data_visualization.pygal.die import Die

die_1 = Die(8)
die_2 = Die(8)

results = []
for roll in range(50000):
    result = die_1.roll() + die_2.roll()
    results.append(result)

# Analyze the result
frequencies = []
max_result = die_1.num_sides + die_2.num_sides
for value in range(2, max_result + 1):
    frequency = results.count(value)
    frequencies.append(frequency)

# Visualize the result
hist = pygal.Bar()
hist._title = "Result of rolling two D6 1000 times."
# hist.x_labels = list(range(2, (max_result+1)))
hist.x_labels = [x for x in range(2, max_result+1)]
hist._x_title = "Result"
hist._y_title = "Frequency"

hist.add("D6 + D6", frequencies)
hist.render_to_file('dice_visual.svg')
