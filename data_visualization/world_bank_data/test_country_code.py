import unittest
from data_visualization.world_bank_data.country_code import get_country_code


class CountryCodeTestCase(unittest.TestCase):
    """Test if the 'get_country_code' method gives correct country code or not."""
    def test_get_country_code_function(self):
        formatted_data = get_country_code("United Arab Emirates")
        self.assertEqual(formatted_data, "ae")


if __name__ == '__main__':
    unittest.main()
