import csv
from pygal.style import LightColorizedStyle as LiCoSt, RotateStyle as RoSt
from pygal_maps_world.maps import World
from data_visualization.world_bank_data.country_code import get_country_code

filename = 'forest_area_sq_km.csv'
with open(filename) as obj:
    # CSV file reader object initialization
    reader = csv.reader(obj)

    # reads first row from the reader object
    header_row = next(reader)
    # print(header_row)
    # for index, column_header in enumerate(header_row):
    #     print(str(index)+" : "+column_header)

    forest_area_2016, country_names, cc_area = [], [], {}

    # reads forest area data and country names of 2016 row-wise and stores in list.
    for row in reader:
        try:
            area = int(float(row[60]))
            country = row[0]
        except ValueError:
            print(area, '2016 Data Missing.')
        else:
            # forest_area_2014.append(area1)
            # forest_area_2015.append(area2)
            forest_area_2016.append(area)
            country_names.append(country)

# Gets Country code and makes dictionary with area as value.
for x in range(256):
    # print(country_names[x]+" : "+str(forest_area_2016[x]))
    country = country_names[x]
    area = forest_area_2016[x]
    code = get_country_code(country)
    if code:
        cc_area[code] = area

# Categorise
cc_area_1, cc_area_2, cc_area_3, cc_area_4 = {}, {}, {}, {}
for cc, area in cc_area.items():
    if 0 <= area < 100000:
        cc_area_1[cc] = area
    elif 100000 < area < 1000000:
        cc_area_2[cc] = area
    elif 1000000 < area < 5000000:
        cc_area_3[cc] = area
    else:
        cc_area_4[cc] = area

print("area1: " + str(len(cc_area_1)) + " area2: " + str(len(cc_area_2)) + " area3: " + str(len(cc_area_3)) +
      " area4: " + str(len(cc_area_4)))
map_style = RoSt('#999999', base_style=LiCoSt)
world_map = World()

world_map._title = "World Forest Area in 2016"
world_map.add("0-10K(sq.km)", cc_area_1)
world_map.add("10K-1M(sq.km)", cc_area_2)
world_map.add("1M-5M(sq.km)", cc_area_3)
world_map.add(">5M(sq.km)", cc_area_4)
world_map.render_to_file("world_forest_area.svg")
