favourite_language = {
    'john': 'Java',
    'bella': 'Python',
    'marry': 'Ruby',
    'Edward': 'C++',
}
for key in favourite_language:
    print(key)

for key in favourite_language.keys():
    print(key)

friends = ['Phil', 'Elena']
for friend in favourite_language.keys():
    if friend in friends:
        print("Hi '"+friend.title()+"', I see your favourite language is '"+favourite_language[friend].title()+'!!')

if 'Eden' not in favourite_language.keys():
    print("Please Eden take our Poll !!")

for name in sorted(favourite_language.keys()):
    print(name.title()+", Thank you for participationg in our Poll!!")
