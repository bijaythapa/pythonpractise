users = {
    'Edward': {
        'first': 'Edward',
        'last': 'Collin',
        'location': 'princeton',
    },
    'bella': {
        'first': 'bella',
        'last': 'swan',
        'location': 'paris',
    },
}
for username, user_info in users.items():
    print("\n"+username.title())
    full_name = user_info['first']+" "+user_info['last']
    location = user_info['location']
    print("\tFullname: "+full_name.title())
    print("\t Location: "+location.title())
