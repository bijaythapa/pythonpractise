favourite_place = {}
for i in range(2):
    name = input("Enter your name: ")
    fav_place = []
    print("Enter your 3 favourite places: ")
    for j in range(3):
        fav_place.append(input())
    favourite_place[name] = fav_place
print(favourite_place)
for name, fav_places in favourite_place.items():
    print("\n"+name.title()+"'s favourite places are: ")
    for fav_place in fav_places:
        print("\t"+fav_place.title())
