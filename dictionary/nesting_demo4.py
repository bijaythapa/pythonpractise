favourite_language = {
    'Jen': ['Python', 'Java'],
    'John': ['Python', 'Ruby'],
    'Merry': ['C#', 'Python'],
    'Phil': ['C'],
    'Edward': ['Go', 'Haskell'],
}
for name, languages in favourite_language.items():
    print("\n" + name.title() + "'s favourite languages are: ")
    if len(languages) > 1:
        for language in languages:
            print("\t" + language.title())
    else:
        print("\n" + name.title() + 's favourite language is: ' + languages[0])
