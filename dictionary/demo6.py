favourite_languages = {
    'jen': 'Python',
    'sarah': 'Java',
    'edward': 'ruby',
    'phil': 'Python',
}
for value in favourite_languages.values():
    print(value)

for value in sorted(favourite_languages.values()):
    print(value)

for value in set(favourite_languages.values()):
    print("value: "+value)
