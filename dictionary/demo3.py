alien_0 = {
    'x_position': 0,
    'y_position': 25,
    'speed': 'fast',
    'color': 'blue',
}
print(alien_0)
if alien_0['speed'] == 'slow':
    alien_0['x_position'] = 5
elif alien_0['speed'] == 'fast':
    alien_0['x_position'] = 15
else:
    alien_0['x_position'] = 10
print(alien_0['x_position'])
# alien_0['speed'] = 'fast'
del alien_0['color']
print(alien_0)

favourite_languages = {
    'jen': 'C',
    'sarah': 'Java',
    'edward': 'ruby',
    'phil': 'python',
}
print("Sarah favourite programming language is: '"+favourite_languages['sarah'].title()+"' .")
