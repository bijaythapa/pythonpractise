favourite_language = {
    'john': 'Java',
    'bella': 'Python',
    'marry': 'Ruby',
    'Edward': 'C++',
}
for coder, language in favourite_language.items():
    print(coder.title()+"'s favourite programming Language is '"+language+"'.")

for key in favourite_language:
    print(key)

for key in favourite_language.keys():
    print(key)
