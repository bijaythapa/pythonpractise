# stored information about pizza being ordered
pizza = {
    'crust': 'thick',
    'toppings': ['mushroom', 'extra cheese']
}

# summarize the order
print("You order a " + pizza['crust'] + "-crust pizza with the following toppings: ")
for topping in pizza['toppings']:
    print("\t" + topping)
