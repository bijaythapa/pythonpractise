major_rivers = dict()
for i in range(3):
    country = input("Enter Country name: ")
    river = input("Enter major River name: ")
    major_rivers[country] = river

for c, r in major_rivers.items():
    print("The major river of '" + c + "' is: '" + r + "'.")

print("The major rivers are: ")
for river in major_rivers.values():
    print(river)
print("The country names are: ")
for country in major_rivers.keys():
    print(country)
