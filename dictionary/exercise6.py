user_0 = {
    'username': 'edward_col',
    'first': 'edward',
    'last': 'collin',
}
user_1 = {
    'username': 'alice_col',
    'first': 'alice',
    'last': 'collin',
}
user_2 = {
    'username': 'bella_sh',
    'first': 'bella',
    'last': 'swan',
}
users = [user_0, user_1, user_2]
for user in users:
    print("\ndetails of: "+str(user))
    print("username: "+user['username']+"\n first name: "+user['first'].title()+"\n last name: "+user['last'].title())

