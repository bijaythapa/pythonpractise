print("German Vocabularies !!")
german_words = {}
for x in range(3):
    word = input("Enter the word: ")
    meaning = input("Enter the meaning: ")
    german_words[word] = meaning
for key, value in german_words.items():
    print(key + " : " + value)
print("Other way: ")
for key in german_words:
    print(key+" : "+german_words[key])
