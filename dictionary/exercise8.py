cities = {
    'Kathmandu': {
        'country': 'Nepal',
        'population': '8273987',
        'fact': 'known as the city of gods.'
    },
    'Pokhara': {
        'country': 'Nepal',
        'population': '773987',
        'fact': 'known as the city of lakes.'
    },
    'Chitwan': {
        'country': 'Nepal',
        'population': '873987',
        'fact': 'known as the city of wild-life.'
    },
}
for city, infos in cities.items():
    print("\n"+city.title()+"'s information: ")
    for info, data in infos.items():
        print("\t"+info+" : "+data)
