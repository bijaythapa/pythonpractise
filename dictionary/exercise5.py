fav_lang = {
    'Bishal': 'Python',
    'Sanjeev': 'Ruby',
    'Mahesh': 'PHP',
    'Durpin': 'C#'
}
print("This is the Favourite Language Poll !!")
print("Enter name of people participating the poll: ")
people = []
for x in range(10):
    people.append(input())
print("People Participating the Poll are: " + str(people))
for name in people:
    if name not in fav_lang.keys():
        print("Mr, '" + name + ", You have not participated in poll. Please participate first !")
        language = input("Enter your Favourite language: ")
        fav_lang[name] = language
        print("Thank you for participating !!")
    elif name in fav_lang.keys():
        print("Mr. " + name + ", Your favourite programming language is: '" + fav_lang[name] + "' .")
    else:
        print("Your are not the member")
print("Final poll Result: ")
for k, v in fav_lang.items():
    print(k + " : " + v)
