from random import randint


class Die:
    """A simple example of Dice."""

    def __init__(self, sides):
        self.sides = sides

    def roll_die(self):
        number = randint(1, self.sides)
        print(number)


dice1 = Die(20)
for roll in range(10):
    dice1.roll_die()
