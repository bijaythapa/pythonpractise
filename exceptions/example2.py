print("Give me two numbers, and I will divide them: ")
print("Enter q to Quit")
while True:
    first_number = input("\n first number: ")
    if first_number == 'q':
        break
    second_number = input("Second Number: ")
    if second_number == 'q':
        break
    try:
        answer = int(first_number)/int(second_number)
    except ZeroDivisionError:
        print("Can not divide by 0 !!")
    else:
        print(answer)
    break

