import json


def get_fav_number():
    """Provide favourite number of user"""
    file_name = 'fab_number.json'
    try:
        with open(file_name) as file_object:
            number = json.load(file_object)
    except FileNotFoundError:
        return None
    else:
        return number


def set_fav_number():
    """store favourite number of user in file"""
    file_name = 'fab_number.json'
    try:
        fav_num = int(input("Enter your Favourite Number:"))
        with open(file_name, 'w') as file_object:
            json.dump(fav_num, file_object)
    except ValueError:
        print("Invalid Input !!")
    except EOFError:
        print("Goodbye !!")


def greet_user():
    """Check if the user's favourite number is exists, if not get from user"""
    fav_num = get_fav_number()
    if fav_num:
        print("Your Favourite number is: " + str(fav_num))
    else:
        set_fav_number()
        print("Your Favourite number is saved !!")


greet_user()
