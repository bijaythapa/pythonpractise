# Analyzing File contents.
file_name = 'laptopFeatures.txt'
try:
    with open(file_name) as file_object:
        contents = file_object.read()
        # print(contents)
except FileNotFoundError:
    message = "This file '"+file_name+"' not found."
    print(message)
else:
    # count the appropriate number of words in file.
    words = contents.split()
    num_words = len(words)
    print("The file '"+file_name+"' has about "+str(num_words)+" words.")
    # print(num_words)
