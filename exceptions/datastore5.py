import json

file_name = 'username.json'
try:
    with open(file_name) as file_object:
        username = json.load(file_object)
except FileNotFoundError:
    with open(file_name, 'w') as file_object:
        json.dump(username, file_object)
    print("We'll remember you '"+username+"' when you come back !")
else:
    print("Welcome back, '"+username+"' !")
