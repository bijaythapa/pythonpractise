import json


def get_stored_username():
    """Get stored username from json file"""
    file_name = 'username.json'
    try:
        with open(file_name) as file_object:
            username = json.load(file_object)
    except FileNotFoundError:
        return None
    else:
        return username


def get_new_username(current_username):
    """Prompt for a new username"""
    # username = input("Enter your Username: ")
    file_name = 'username.json'
    with open(file_name, 'w') as file_object:
        json.dump(current_username, file_object)
    return current_username


def greet_user():
    """Greet the user by Username"""
    current_user = input("What is your Name? ")
    username = get_stored_username()
    if username == current_user:
        print("Welcome back '" + current_user + "' !")
    else:
        get_new_username(current_user)
        print("We will remember you when you comeback, '" + current_user + "' !")


greet_user()
