def count_word(file):
    try:
        with open(file) as file_object:
            contents = file_object.read()
        word = input("Enter the word you want to search: ")
    except FileNotFoundError:
        pass
    except EOFError:
        print("Thank you for participating !!")
    else:
        no = contents.count(word)
        print("No of '"+word+"' in files is: " + str(no))
        num = contents.lower().count(word)
        print("No of lower '"+word+"' in files is: " + str(num))


file_name = 'projectgutenberg.txt'
count_word(file_name)
