def count_words(file_name):
    try:
        with open(file_name) as file_object:
            contents = file_object.read()
    except FileNotFoundError:
        pass
    else:
        words = contents.split()
        no_words = len(words)
        print("total words in file '" + file + "' is approximately: '" + str(no_words) + "'.")


file_names = ['dhoom.txt', 'kakarsha.txt', 'bangbang.txt', 'laptopFeatures.txt']
for file in file_names:
    count_words(file)
