while True:
    try:
        first_number = int(input("Enter first Number: "))
        second_number = int(input("Enter second Number: "))
    except ValueError:
        message = "Please enter numeric value !!"
        print(message)
    except EOFError:
        print("Thank you for your time !!")
        break
    else:
        sum_value = first_number + second_number
        print("The summed value is: " + str(sum_value))
