def count_words(file_name):
    """count the number of words in the file"""
    try:
        with open(file_name) as file_object:
            contents = file_object.read()
    except FileNotFoundError:
        message = "The file '" + file_name + "' not found."
        print(message)
    else:
        words = contents.split()
        no_words = len(words)
        print("Total number of words is approximately '" + str(no_words) + "' in '" + file_name + "'.")


file_names = ['dhoom.txt', 'kakarsha.txt', 'bangbang.txt', 'laptopFeatures.txt']
for file in file_names:
    count_words(file)
