# file_name = '../filehandling/guest_book.txt'
file_name = 'alice.txt'
try:
    with open(file_name) as obj:
        contents = obj.read()
        print(contents)
except FileNotFoundError:
    message = "Sorry the file '"+file_name+"' does not Exists !!"
    print(message)
