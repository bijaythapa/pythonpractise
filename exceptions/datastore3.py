import json

file_name = 'username.json'
try:
    username = input("Enter your Name: ")
    with open(file_name, 'w') as file_object:
        json.dump(username, file_object)
except FileNotFoundError:
    pass
except TypeError:
    pass
else:
    print("We'll remember you when you comeback, "+username+" !")
