import json

file_name = 'username.json'
try:
    with open(file_name) as file_object:
        contents = json.load(file_object)
except FileNotFoundError:
    pass
else:
    print("Welcome back, "+contents+" !")