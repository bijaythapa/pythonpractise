from oop_concepts.dog import Dog

my_dog = Dog('Willie', 6)
your_dog = Dog('Lucy', 5)
print("My dog name is: "+my_dog.name.title()+".")
print("My dog age is: "+str(my_dog.age)+".")
my_dog.sit()
my_dog.roll_over()
print("\nMy dog name is: "+your_dog.name.title()+".")
print("My dog age is: "+str(your_dog.age)+".")
your_dog.sit()
your_dog.roll_over()
