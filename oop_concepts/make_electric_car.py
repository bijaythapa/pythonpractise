from oop_concepts.electric_car import ElectricCar

my_tesla = ElectricCar('Tesla', 'Model s', 2016)
print(my_tesla.get_descriptive_name())

# my_tesla.describe_battery()
my_tesla.battery.describe_battery()

my_tesla.battery.get_range()
my_tesla.battery.upgrade_battery()
my_tesla.battery.get_range()

my_tesla.fill_gas_tank()
