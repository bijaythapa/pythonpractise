from oop_concepts.user import User

user1 = User('Bijay', 'Thapa', 23, 'Dhading', 'Python')
user2 = User('Amar', 'Singh', 17, 'Kathmandu', 'Java')
user3 = User('Jyoti', 'Nepal', 21, 'Pokhara', 'Ruby')

user1.greet_user()
user1.describe_user()

user1.increment_login_attempts()
user1.increment_login_attempts()
user1.increment_login_attempts()
print(user1.login_attempt)

user1.reset_login_attempt()
print(user1.login_attempt)

user2.greet_user()
user2.describe_user()

user3.greet_user()
user3.describe_user()
