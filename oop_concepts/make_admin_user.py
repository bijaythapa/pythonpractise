from oop_concepts.user import User, Privileges


class Admin(User):
    """Admin User sample."""

    def __init__(self, first, last, age, address, fav_language, privileges):
        super().__init__(first, last, age, address, fav_language)
        self.privileges = Privileges(privileges)


admin_user = Admin('Will', 'Smith', 23, 'Kathmandu', 'Python', ['can add post', 'can delete post', 'can edit '
                                                                                                   'post'])
# admin_user.show_privileges()
admin_user.privileges.show_privileges()
admin_user.describe_user()
