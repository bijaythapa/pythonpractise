from oop_concepts.car import Car, Battery


class ElectricCar(Car):
    def __init__(self, make, model, year):
        super().__init__(make, model, year)
        # self.battery_size = 70
        """Instances as Attribute."""
        self.battery = Battery()

    # def describe_battery(self):
    #     print("This car has a "+str(self.battery_size)+"-kWh battery.")

    def fill_gas_tank(self):
        """Electric car doesnt have gas tank."""
        print("This car doesnt need gas tank.")
