from oop_concepts.restaurant import Restaurant

restaurant = Restaurant('The Malla Hotel', 'Chinese')
print(restaurant.name)
print(restaurant.cuisine)

restaurant.set_served_number(55)
print(restaurant.number_served)
restaurant.increment_served_number(15)
print("a day of business: "+str(restaurant.number_served))

restaurant.describe_restaurant()
restaurant.open_restaurant()

restaurant1 = Restaurant('Radisson Hotel', 'American')
restaurant2 = Restaurant('Hotel Yak and Yeti', 'Italian')
restaurant3 = Restaurant('Soltee Hotel', 'Nepali')

restaurant1.describe_restaurant()
restaurant2.describe_restaurant()
restaurant3.describe_restaurant()
