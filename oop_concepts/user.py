class User:
    """A simple attempt to model User Information"""
    def __init__(self, first, last, age, address, fav_language):
        self.first_name = first
        self.last_name = last
        self.age = age
        self.address = address
        self.favourite_language = fav_language
        self.login_attempt = 0

    def describe_user(self):
        print("The User Information: ")
        user_info = [
            self.first_name,
            self.last_name,
            self.age,
            self.address,
            self.favourite_language,

        ]
        for info in user_info:
            print(info)

    def greet_user(self):
        print("\nHello, "+self.first_name.title()+" "+self.last_name.title())

    def increment_login_attempts(self):
        self.login_attempt += 1

    def reset_login_attempt(self):
        self.login_attempt = 0


class Privileges:
    def __init__(self, privileges):
        self.privileges = []
        for privilege in privileges:
            self.privileges.append(privilege)

    def show_privileges(self):
        print("Privileges for a Admin users are: ")
        for privilege in self.privileges:
            print(privilege)
