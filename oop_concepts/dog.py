class Dog:
    """A simple attempt to model a Dog."""
    def __init__(self, name, age):
        """Initialize name and age attribute"""
        self.name = name
        self.age = age

    def sit(self):
        """Simulate a dog sitting in response to a command"""
        print(self.name.title()+"is now sitting.")

    def roll_over(self):
        """Simulate roll over in response to a command"""
        print(self.name.title()+" rolled over.")
