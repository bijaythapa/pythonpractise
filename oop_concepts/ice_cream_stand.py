from oop_concepts.restaurant import IceCreamStand

ice_cream_stand = IceCreamStand("Himalayan Ice cream", 'American', ['Butterscotch', 'strawberry', 'chocolate'])
ice_cream_stand.display_flavours()
