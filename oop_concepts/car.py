class Car:
    """A simple attempt to represent a car."""

    def __init__(self, make, model, year):
        """Initialize attributes to describe a car."""
        self.make = make
        self.model = model
        self.year = year
        self.odometer_reading = 35

    def get_descriptive_name(self):
        """Return a neatly formatted descriptive name."""
        long_name = str(self.year) + ' ' + self.make + ' ' + self.model
        return long_name.title()

    def read_odometer(self):
        """Print statement that shows the car's mileage"""
        print("This car has "+str(self.odometer_reading)+" miles on it.")

    def update_odometer(self, mileage):
        """Set odometer to the given value. Reject the change if it attempts to roll odometer back."""
        if mileage >= self.odometer_reading:
            self.odometer_reading = mileage
        else:
            print("You cant rollback an odometer.")

    def increment_odometer(self, miles):
        """Add given amount to the odometer reading."""
        self.odometer_reading += miles

    def fill_gas_tank(self):
        print("Filling gas tank.")


class Battery:
    """A simple attempt to model a battery for an electric car."""

    def __init__(self, battery_size = 70):
        """Initialize the battery attributes."""
        self.battery_size = battery_size

    def describe_battery(self):
        print("This car has a "+str(self.battery_size)+"-kWh battery.")

    def get_range(self):
        """Print a statement about the range this battery provides."""
        distance = 0
        if self.battery_size == 70:
            distance = 240
        elif self.battery_size == 85:
            distance = 270

        message = "This car can go approximately "+str(distance)
        message += " miles on a full charge."
        print(message)

    def upgrade_battery(self):
        if self.battery_size != 85:
            self.battery_size = 85
        print("Battery Upgraded.")
