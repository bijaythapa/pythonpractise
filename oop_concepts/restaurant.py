class Restaurant:
    """A simple attempt to model a Restaurant"""
    def __init__(self, restaurant_name, cuisine_type):
        self.name = restaurant_name
        self.cuisine = cuisine_type
        self.number_served = 15

    def describe_restaurant(self):
        print("The restaurant name is: '"+self.name.title()+"' which provides '"+self.cuisine.title()+"' cuisine.")

    def open_restaurant(self):
        print("'"+self.name.title()+"' is open for Business.")

    def set_served_number(self, served):
        self.number_served = served

    def increment_served_number(self, inc):
        self.number_served += inc


class IceCreamStand(Restaurant):
    """A ice cream stand in a Restaurant."""
    def __init__(self, restaurant_name, cuisine_type, flavours):
        super().__init__(restaurant_name, cuisine_type)
        self.flavours = []
        for flavour in flavours:
            self.flavours.append(flavour)

    def display_flavours(self):
        print("Ice Cream Flavours are: ")
        for flavour in self.flavours:
            print(flavour)
