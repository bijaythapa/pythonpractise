from datetime import date
from datetime import time
from datetime import datetime
from datetime import timedelta


def main():
    today = date.today()
    print("Today's date: ", today)
    print("Date Components: ", today.day, today.month, today.year, today.weekday())

    td = time.tzinfo
    print("Tz: ", td)

    abc = datetime.time(datetime.now())
    print("The current time is : ", abc)

    print(dir(datetime))
    print(dir(date))
    print(dir(time))


if __name__ == "__main__":
    main()
