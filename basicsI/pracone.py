import sys
import datetime

print("Twinkle, Twinkle little star \n\t How i wonder what you are! \n\t\t Up above the world so high, \n\t\t Like a "
      "diamond in the sky. \n Twinkle, twinkle, little star, \n\t How i wonder what you are ")
print("---------------------------------------")
print("Python Version")
print(sys.version)
print("Python Version Info")
print(sys.version_info)

print(sys.version_info[0])
print(sys.version_info[1])
print(sys.version_info[2])
print(sys.version_info[3])
print(sys.version_info[4])

print(sys.version_info.major)
print(sys.version_info.minor)
print(sys.version_info.micro)
print(sys.version_info.releaselevel)
print(sys.version_info.serial)
print("------------------------")

now = datetime.datetime.now()
print(now)
print("Current Date and Time: ")
print(now.strftime("%Y-%m-%d-%day-%A %H:%M:%s"))
print("Year: ", now.year)
print("Month: ", now.month)
print(now.strftime("%A"))

never = datetime.datetime(2020, 5, 30)  # kind of setting datetime values in a variable
print("Never:", never)

tod = datetime.time
print(tod)
