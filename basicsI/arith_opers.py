# a = 10
# b = 2
# print('a+b=', a + b)
# print('a-b=', a - b)
# print('a*b=', a * b)
# print('a/b=', a / b)
# print('a//b=', a // b)
# print('a%b=', a % b)
# print('a**b=', a ** b)
# print(type(a/b))

# a = "hello"
# print(a.count("l"))
# print(a.count(""))

# b = 2
#
#
# def py():
#     global b
#     b = 3
#
#
# print(b)


# def foo(x):
#     x[0] = ['def']
#     x[1] = ['abc']
#     return id(x)
#
#
# q = ['abc', 'def']
# print(id(q))
# print(id(q) == foo(q))


# def function(a, b, c=1, d=5):
#     return a + b + c + d
#
#
# value = function(1, 2, d=7)
# print(value)

# a = {
#     1: 2,
#     'list': [1, 2],
#     3: 5,
# }
# a.pop('list')
# a['list'] = [3, 5]
# print(a['list'])
# print(type(a))

# a = [3, 4, 5, 6, 7, 8, 9]
# print(a.index(3))
# print(a.index(4))
# print(a.index(7))

for num in range(-2, -5, -1):
    print(num, end=",")
