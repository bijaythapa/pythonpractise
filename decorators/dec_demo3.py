def decorator_maker(arg1, arg2, arg3):
    def decorator(function):
        def wrapper_func(fun_arg1, fun_arg2, fun_arg3):
            print("Wrapper can access all variables \n \t - from Decorator maker: {0} {1} {2} \n \t"
                  " - from Function call: {3} {4} {5}\n and Pass them to Decorated function"
                  .format(arg1, arg2, arg3, fun_arg1, fun_arg2, fun_arg3))
            return function(arg1, arg2, arg3)
        return wrapper_func
    return decorator


@decorator_maker("Hibernate", "Spring", "Spring-Boot")
def dec_func_with_args(arg1, arg2, arg3):
    print("This is the decorated function and it only knows about its arguments: {0} {1} {2}"
          .format(arg1, arg2, arg3))


dec_func_with_args("John", 12, 13.45)
