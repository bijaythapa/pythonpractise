def decorator_args(function):
    def wrapper_func(arg1, arg2):
        print("Arguments are: {0} and {1}".format(arg1, arg2))
        function(arg1, arg2)

    return wrapper_func


@decorator_args
def districts(dis_one, dis_two):
    print("Districts are: {0} and {1}".format(dis_one, dis_two))


districts("Dhading", "Chitwan")


def decorator_gen(function):
    def wrapper_func(*args, **kwargs):
        print("Positional arguments are: ", args)
        print("Positional arguments are: ", kwargs)
        function(*args)
    return wrapper_func


@decorator_gen
def function_with_kwarg():
    print("It contains Keyword Arguments !!")


function_with_kwarg(first_name="bijay", faculty="Computer Science", Roll=99)


