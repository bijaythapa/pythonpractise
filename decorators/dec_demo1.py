# print("Assigning Method to variables: ")
#
#
# def summing(num):
#     return num + 1
#
#
# sum_one = summing
# # sum_one(10)
# print(sum_one(12))
#
# print("Defining Functions Inside other Functions: ")
#
#
# def sum_one(num):
#     def add_one(numb):
#         return numb + 1
#
#     summation = add_one(num)
#     return summation
#
#
# print(sum_one(22))

# print("Passing Functions as Arguments to other Functions: ")
#
#
# def sum_one(num):
#     return num + 1
#
#
# def add_one(function):
#     num_to_add = 40
#     return function(num_to_add)
#
#
# # add_one(sum_one)
# print(add_one(sum_one))

# print("Passing Functions as Arguments to other Functions: ")
#
#
# def say_hello():
#     def say_hi():
#         return "Hi, "
#     return say_hi
#
#
# hello = say_hello()
# print(hello())

# print("Nested Functions have access to the Enclosing Function's Variable Scope: ")
#
#
# def print_me(msg):
#     def message_send():
#         print(msg)
#
#     message_send()
#
#
# print_me("hello from the other side !!")

print("Nested Functions have access to the Enclosing Function's Variable Scope: ")


def change_lower(change):
    def wrapper():
        func = change()
        make_lowercase = func.lower()
        return make_lowercase
    return wrapper


def split_to(change):
    def wrapper():
        func = change()
        split_msg = func.split()
        return split_msg
    return wrapper


@split_to
@change_lower
def say_hello():
    return "How ARE You?"


print(say_hello())


# decor = change_lower(say_hello)
# decor()
# print(decor())
