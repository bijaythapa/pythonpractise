# def addValue(x, y):
#     return x+y

add = lambda a, b: a+b
print(add(5, 10))

print((lambda x,y:x+y)(11,12))

tuples = [(1,'d'), (2,'b'), (4,'a'), (3,'c')]
print(sorted(tuples, key=lambda x:x[1]))

print(sorted(range(-5,6), key= lambda x: x*x))
