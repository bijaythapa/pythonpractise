errno = 50159747054
name = 'Bob'
# 'Hey Bob, there is a 0xbadc0ffee error!'
print('Hello %s, there is a 0x%x error!' % (name, errno))

print('Hello, %s' % name)

print('%x' % errno)

print('Hello %(name)s, there is a 0x%(errno)x error!' % {"name": name, "errno": errno})
