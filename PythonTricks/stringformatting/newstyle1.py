name = 'Bob'
errno = 50159747054

print('Hello, {}'.format(name))
print('Hey {name}, there is a 0x{error:x} error!'.format(name=name, error=errno))
