import dis


def greet(nam, question):
    return f"Hello {nam}! How's it {question}?"


string = greet('Bob', 'going')
print(string)

dis.dis(greet)

name = 'Bob'
errno = 50159747054
print(f"Hey {name}, there's a {errno:#x} error!")

