from string import Template

name = 'Bob'
errno = 50159747054
temp_string = "Hello $name, there's a $errno error!"
string1 = Template(temp_string).substitute(name=name, errno=hex(errno))
print(string1)
