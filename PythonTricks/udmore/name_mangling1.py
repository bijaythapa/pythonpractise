class Test:
    def __init__(self):
        foo = 200
        self._bar = 23
        self.__baz = 23


class ExtendedTest(Test):
    def __init__(self):
        super().__init__()
        self.foo = 'overridder'
        self._bar = 'overridden'
        self.__baz = 'overriddern'


t = Test()
print(dir(t))
# print(t.foo)
# print(t._bar)
# print(t.__baz)

t2 = ExtendedTest()
print(dir(t2))
print(t2.foo)
print(t2._bar)

# this gives no attribute error
# print(t2.__baz)

print(t2._ExtendedTest__baz)
print(t2._Test__baz)

