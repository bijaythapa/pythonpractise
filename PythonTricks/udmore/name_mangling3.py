class MangledMethod:
    def __method(self):
        return 42

    def call_it(self):
        return self.__method()


# gives object has no attribute __method error
# print(MangledMethod.__method())
x = MangledMethod().call_it()
print(x)
