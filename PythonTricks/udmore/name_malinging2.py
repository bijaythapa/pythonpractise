class ManglingTest:
    # __mangled = None

    def __init__(self):
        self.__mangled = "mangled attribute"

    def get_mangled(self):
        return self.__mangled


print(ManglingTest().get_mangled())


# this also give object has no attribute error
# print(ManglingTest.__mangled)
