def get_speak_func(text, value):
    def whisper():
        return text.upper()

    def yell():
        return text.lower()

    if value > 0.5:
        return yell
    else:
        return whisper


def make_adder(n):
    def add(x):
        return x+n
    return add


from_whisper = get_speak_func("Hello World", 0.2)()
from_yell = get_speak_func("HELLO WORLD", 0.7)()

print(from_whisper)
print(from_yell)

plussing = make_adder(5)
output = plussing(3)
print(output)
output = plussing(5)
print(output)
