def whisper(value):
    return value.lower() + "!"


def shout(value):
    return value + ' shouted and greeted'


def greet(func):
    greeting = func(text)
    print(greeting)


text = input("Whisper here: ")
# this allows us to abstract away and pass around 'behaviour'
# like: greet functions stays the same but we can influence out by passing different 'greeting behaviours'
greet(whisper)
greet(shout)
