def yell(value):
    return value.upper()+"!"


bark = yell
# map takes: a function object and an iterable.
# calls function for each item of iterable and yields the result as it goes along
funcs = list(map(bark, ['hello', 'hey', 'hi']))

# returns results in same form of iterable.
print(funcs)
