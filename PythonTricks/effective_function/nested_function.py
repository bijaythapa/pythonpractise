def speak(value):
    def whisper(val):
        return val.lower() + "..."

    return whisper(value)


print(speak("Hello World"))


def get_speak_func(volume):
    def whisper(text):
        return text.upper()

    def yell(text):
        return text.lower()

    if volume > 0.5:
        return yell
    else:
        return whisper


print(get_speak_func(0.7))
print(get_speak_func(0.2))

whisper_func = get_speak_func(0.2)
yell_func = get_speak_func(0.8)

print(whisper_func("hello world"))
print(yell_func('HELLO WORLD'))
