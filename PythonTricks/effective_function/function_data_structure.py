def yell(value):
    return value.upper() + '!'


shout = yell
# we can store function in a data structure like other objects
functions = [shout, str.lower, str.capitalize]

# also we can access them like the other objects
for func in functions:
    # print(func)
    # print(func('hang in there'))
    print(func, func('hang in there'))

text = functions[0] = 'check this'
print(text)
