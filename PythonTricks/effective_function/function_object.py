def yell(value):
    return value.upper()+"!"


text = 'shine on you crazy diamond'
result = yell(text)
print(result)

# Because 'yell' function is object in Python, we can assign it to another variable, just like other object.
shout = yell
# here, shout stores the yell function object-reference and points to it.
# so now, we can execute the same underlying function object by calling the variable 'shout'
output = shout('wish you were here')
print(output)

# Proof: the shout stores the function object reference
print(yell)
print(shout)

# Function Objects and Names are different concerns.
# Proof: even if we delete name 'yell', we can still call the function through another name 'shout' because it still
# points to the underlying function
del yell
# print(yell)
print(shout)
output = shout("knocing on the heaven's door")
print(output)

# Python attaches a string identifier to every function at creation time for debugging purpose.
# we can access this internal identifier with '__name__' attribute.
print(shout.__name__)
