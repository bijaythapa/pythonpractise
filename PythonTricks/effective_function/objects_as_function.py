class Adder:
    def __init__(self, n):
        self.n = n

    def __call__(self, x, *args, **kwargs):
        return self.n + x


plus_3 = Adder(5)

output = plus_3(10)
print(output)
print(callable(plus_3))
