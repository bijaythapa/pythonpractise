def yell(text):
    return text.upper() + "!"


value = 'Hello from the outside'
output = yell(value)
print(output)
