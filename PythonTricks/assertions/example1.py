def apply_discount(product, discount):
    price = int(product['price'] * (1.0 - discount))
    assert 0 <= price <= product['price']
    # here this assertion guarantee that the discounted price cannot be lower than $0
    # and they cannot be higher than the original price of the product
    return price


shoes = {
    'name': 'Fancy Shoes',
    'price': 14500,
}
x = apply_discount(shoes, 2.0)
print(x)
