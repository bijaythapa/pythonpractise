# use of 'with' in file handling
with open('hello.txt', 'w') as f:
    f.write(', world')

with open('hello.txt') as f:
    content = f.read()
    print(content)
print('-----------------------------')
file = open('hello.txt', 'w')
try:
    file.write(' from the other side')
finally:
    file.close()

r_file = open('hello.txt')
try:
    content = r_file.read()
    print(content)
finally:
    r_file.close()
print('----------------------------')
f = open('hello.txt', 'w')
f.write(' lets have some coffee')
f.close()
