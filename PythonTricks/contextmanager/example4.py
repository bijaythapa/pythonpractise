from contextlib import contextmanager


@contextmanager
def managed_file(file):
    try:
        f = open(file, 'w')
        yield f
    finally:
        f.close()


with managed_file('hello.txt') as file_object:
    file_object.write('hello, world')
    file_object.write('bye bye !!')
