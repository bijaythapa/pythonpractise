# with in threading.Lock class
import threading

some_lock = threading.Lock()

# Harmful
some_lock.acquire()
try:
    print(2+3)
finally:
    some_lock.release()

# better
with some_lock:
    print(5+6)
