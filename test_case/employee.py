class Employee:
    """Employee salary sheet."""
    def __init__(self, first, last, salary):
        self.first_name = first
        self.last_name = last
        self.annual_salary = salary

    def give_default_raise(self):
        self.annual_salary += 5000

    def give_custom_raise(self, raise_salary):
        self.annual_salary += raise_salary
