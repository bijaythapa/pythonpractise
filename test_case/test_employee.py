import unittest
from test_case.employee import Employee


class EmployeeTestCase(unittest.TestCase):
    """Tests if employee salary increases or not."""
    def setUp(self):
        self.my_employee = Employee('Edward', 'Collins', 40404)

    def test_give_default_raise(self):
        self.my_employee.give_default_raise()
        self.assertEqual((40404+5000), self.my_employee.annual_salary)

    def test_give_custom_raise(self):
        self.my_employee.give_custom_raise(2222)
        self.assertEqual((40404+2222), self.my_employee.annual_salary)


if __name__ == '__main__':
    unittest.main()
