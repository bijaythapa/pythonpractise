import unittest
from test_case.city_functions import city_name


class CityNamesTestCase(unittest.TestCase):
    """Tests city and country name in format: city, country."""

    def test_city_country(self):
        formatted_name = city_name("chitwan", 'nepal', 4444444)
        self.assertEqual(formatted_name, "Chitwan, Nepal- 4444444")

    def test_city_country_population(self):
        formatted_name = city_name("kathmandu", 'nepal', 5555555)
        self.assertEqual(formatted_name, 'Kathmandu, Nepal- 5555555')
