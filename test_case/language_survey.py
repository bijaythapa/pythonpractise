from test_case.survey import AnonymousSurvey

# Define a question and make survey.
question = "What language did you first learn to speak? "
my_survey = AnonymousSurvey(question)

# show the question and store responses to the question
my_survey.show_question()
print("Enter 'q' any time to quit.")
while True:
    response = input("Language: ")
    if response == 'q':
        break
    my_survey.store_response(response)

# show the survey result
print("Thank you for participating on the survey.")
my_survey.show_result()
