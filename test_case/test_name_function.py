import unittest
from test_case.name_function import get_formatted_name


class NamesTestCase(unittest.TestCase):
    """Tests for 'name_function.py'."""

    def test_first_last_name(self):
        """Do names like 'Janis Joplin works?"""
        formatted_name = get_formatted_name('janis', 'ley', 'joplin')
        self.assertEqual(formatted_name, 'Janis Ley Joplin')

    def test_first_middle_last_name(self):
        """Do names like 'Dal Lee Collins' works?"""
        formatted_name = get_formatted_name('dal', 'lee', 'collins')
        self.assertEqual(formatted_name, 'Dal Lee Collins')


# unittest.main()
