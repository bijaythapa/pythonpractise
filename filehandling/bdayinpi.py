filename = "pi_digits.txt"
with open(filename) as file_object:
    lines = file_object.readlines()

pi_string = ''
for line in lines:
    # print("line: "+line)
    pi_string += line.rstrip()

# print("pi string: "+pi_string)
# print(type(pi_string))
birthday = input("Enter your Birthday, in the form 'mmddyy': ")
if birthday in pi_string:
    print("Your birthday appears in the first million digits of Pi !!")
else:
    print("Your birthday does not appear in the first million digits of Pi.")
