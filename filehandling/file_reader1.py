with open('pi_digits.txt') as file_object:
    data = file_object.read()
    print(data)
    # read() returns an empty string when it reaches end of file; this empty string shows-up as blank line.
    # so to remove the blank line we use rstrip() method.
    # rstrip(): removes/strips any whitespace characters from the right-side of a string.
    print(data.rstrip())
