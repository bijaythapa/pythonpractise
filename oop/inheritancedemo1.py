class Animal:
    def __init__(self):
        print("Animal created")

    def who(self):
        print("Animal")

    def eat(self):
        print("Eating")


class Dog(Animal):
    def __init__(self):
        Animal.__init__(self)
        print("Dog Created")

    def bark(self):
        print("wooof Woof")

    def eat(self):
        print("Dog Eating !!")


my_animal = Animal()
my_animal.who()
my_animal.eat()

my_dog = Dog()
my_dog.who()
my_dog.eat()
my_dog.bark()
my_dog.eat()