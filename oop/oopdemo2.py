class Circle:
    pi = 3.14

    def __init__(self, radius=1):
        self.radius = radius

    def area(self):
        return self.radius * self.radius * Circle.pi

    def set_radius(self, new_r):
        self.radius = new_r


myCir = Circle(3)
print(myCir.radius)
print(myCir.area)
print(myCir.area())

myCir.radius = 5
print(myCir.area())

myCir.set_radius(100)
print(myCir.area())
