# Special Method


class Book:
    def __init__(self, title, author, pages):
        self.title = title
        self.author = author
        self.page = pages

    def __str__(self):
        return "Title: {}, Author: {}, Pages: {}".format(self.title, self.author, self.page)

    def __len__(self):
        return self.page

    def __del__(self):
        print("a book is destroyed !!")


b = Book("Two Scoops", "Jose", 555)
print(b)
print(len(b))
print(type(b))

# mylist = [1, 2, 3]
# print(mylist)
