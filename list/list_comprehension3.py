numbers = [num for num in range(1, 20, 2)]
print(numbers)
for num in numbers:
    print(num)

multiples = [value*3 for value in range(1, 11)]
print(multiples)

cubes = [cube**3 for cube in range(1, 11)]
print(cubes)
