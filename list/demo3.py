try:
    num = int(input("How many friends you want to add in a List? "))
    friend_list = []
    print("Enter your Friends name one-by-one: ")
    for n in range(num):
        name = input()
        friend_list.append(name)
    print(friend_list)
    friend_list.sort()
    print("sorted list:"+str(friend_list))
    friend_list.sort(reverse=True)
    print("reverse sort list:"+str(friend_list))
    print(sorted(friend_list, reverse=True))
    print(sorted(friend_list))

except TypeError:
    print("Please Enter numeric value !!")
