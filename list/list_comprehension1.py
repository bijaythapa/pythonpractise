squares = [value ** 2 for value in range(1, 11)]
print(squares)

numbers = [value for value in range(1, 21)]
print(numbers)
