murakami_books = [
    'Norwegian Wood',
    'Kafka on the Shore',
    'Killing Commendatore',
    'the wind-up bird chronicle',
    '1Q84',
    'Men without Women',
]
print(murakami_books)
# this way copies all the data even if we later append value in copied list.
haruki_books = murakami_books
print(haruki_books)

harikami_books = murakami_books[:]
print(harikami_books)

murakami_books.append('Dance Dance Dance')
print(murakami_books)
print(haruki_books)
print(harikami_books)
