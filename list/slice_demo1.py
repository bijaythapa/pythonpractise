players = ['christiano', 'dybala', 'higuine', 'bernadheski', 'costa', 'pjanic', 'matuidi', 'bentancur']
print(players[1:5])
print(players[:5])
print(players[3:])
print(players[-4:])
print(players[:-4])

print("Here is the first 5 players of Juventus: ")
for player in players[:6]:
    print(player)

print("Here is the list of last 4 players of juventus")
for player in players[:4]:
    print(player)
