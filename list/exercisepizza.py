pizza_types = list()
print("How many pizza types are your Favourite?")
fav_no = int(input())
print("Now, Enter your favourite pizza names one-by-one: ")
for x in range(fav_no):
    pizza_type = input()
    pizza_types.append(pizza_type)
print("Your favourite pizza types are: ")
for pizza_type in pizza_types:
    print("I like '"+pizza_type+"' pizza.")
print("My favourite pizza among the list is Pepperoni Pizza.\nI also like Margherita pizza but my second"
      "favourite is BBQ Chicken pizza.\n Honestly saying, I really love Pizza, any types actually.")

friend_pizza = pizza_types[:]
pizza_types.append('new_pizza')
friend_pizza.append('new_friend_pizza')
print("My favourite pizza are: ")
for pizza in pizza_types:
    print(pizza)
print("My friend favourite pizza are: ")
for pizza in friend_pizza:
    print(pizza)
