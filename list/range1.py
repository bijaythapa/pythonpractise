numbers = list(range(1, 6))
print(numbers)

even_numbers = list(range(0, 29, 2))
print(even_numbers)

odd_numbers = list(range(1, 20, 2))
print(odd_numbers)

squares = []
for value in range(1, 11):
    squares.append(value**2)
print("The square of first 10 numbers are: ")
print(squares)

print("Minimum number is: "+str(min(squares)))
print("Maximum number is: "+str(max(squares)))
print("Total sum is: "+str(sum(squares)))
